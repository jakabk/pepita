<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ChartController extends Controller {

   public function index() {
    
       return view('chart');
   } 
   public function getConsoleLogsChartData() {

        $dayLimit = 32;

      $logData = DB::table('public_console_logs')
      ->whereNotNull('run_time')
      ->whereDate('start_at', '>=', Carbon::now()->subDay($dayLimit))
      ->limit(1000)
      ->get(['command','start_at','finished_at', 'run_time']);


      $out = [];
      $out['categories'] = array_values(array_unique(array_column($logData->toArray(), 'command')));
      foreach($logData as $item){
          $start_at = strtotime($item->start_at);
          $out['data'][]= [
                'name' => $item->command,
                'value' =>[
                    $item->command,
                    $start_at,
                    $start_at+$item->run_time,
                    $item->run_time
                   ]
                ];
                
      }
      return response()->json(array(
          'logData'=> $out,
        ), 200);
   }
}