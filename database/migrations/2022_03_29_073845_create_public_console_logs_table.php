<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_console_logs', function (Blueprint $table) {
            
           $table->id(); 
           $table->text('command')->nullable(false);
           $table->json('options');
           $table->text('status')->nullable(false);
           $table->text('error');
           $table->timestamptz('start_at')->default(DB::raw('CURRENT_TIMESTAMP'))->nullable(false);
           $table->bigInteger('start_mem_total')->nullable();
           $table->bigInteger('start_mem_free')->nullable();
           $table->timestamptz('finished_at')->nullable();
           $table->bigInteger('finished_mem_total')->nullable();
           $table->bigInteger('finished_mem_free')->nullable();
           $table->bigInteger('run_time')->nullable();
           $table->text('server')->nullable();       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pepita_public_console_logs');
    }
};
