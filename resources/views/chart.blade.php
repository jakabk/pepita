<html>

<head>
   <title>Console Logs Chart</title>

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
   </script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.0.1/echarts.min.js"></script>

   <script>
      $(document).ready(function() {
         $.ajax({
            type: 'POST',
            url: "{{route('get_console_logs_chart_data')}}",
            data: {
               "_token": "{{ csrf_token() }}",
               "id": "kecske"
            },
            success: function(result) {
               
               console_logs_chart(result);
            }
         });
      });

      function console_logs_chart(data) {
        
         console.log(data.logData.categories);

         var chartDom = document.getElementById('console_log_chart');

         var myChart = echarts.init(chartDom);
         var option;

         
         var dataCount = 10;
         var startTime = +new Date();
         
         function renderItem(params, api) {
           
            var categoryIndex = api.value(0);
            var start = api.coord([api.value(1), categoryIndex]);
            var end = api.coord([api.value(2), categoryIndex]);
            var height = api.size([0, 1])[1] * 0.6;
            var rectShape = echarts.graphic.clipRectByRect({
               x: start[0],
               y: start[1] - height / 2,
               width: end[0] - start[0],
               height: height
            }, {
               x: params.coordSys.x,
               y: params.coordSys.y,
               width: params.coordSys.width,
               height: params.coordSys.height
            });
            return (
               rectShape && {
                  type: 'rect',
                  transition: ['shape'],
                  shape: rectShape,
                  style: api.style()
               }
            );
         }
         option = {
            tooltip: {
               formatter: function(params) {
                  return params.marker + params.name + ': ' + params.value[3] + ' s';
               }
            },
            title: {
               text: 'Console Logs Chart',
               left: 'center'
            },
            dataZoom: [{
                  type: 'slider',
                  filterMode: 'weakFilter',
                  showDataShadow: false,
                  top: 700,
                  labelFormatter: ''
               },
               {
                  type: 'inside',
                  filterMode: 'weakFilter'
               }
            ],
            grid: {
               height: 600
            },
            xAxis: {
               min: 1646009975,
               scale: true,
               axisLabel: {
                  formatter: function(val) {
                     return Math.max(0, val - 1646009975) + ' s';
                  }
               }
            },
            yAxis: {
               data: data.logData.categories
            },
            series: [{
               type: 'custom',
               renderItem: renderItem,
               itemStyle: {
                  opacity: 0.8
               },
               encode: {
                  x: [1, 2],
                  y: 0
               },
               data: data.logData.data
            }]
         };

         option && myChart.setOption(option);

      }
   </script>
</head>

<body>
   <div id="console_log_chart" style="height:800px;border:1px solid #ccc;padding:10px;">
   </div>

</body>

</html>